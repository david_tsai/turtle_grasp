
import numpy as np
import metrics
import sys

class GraspPlanner(object):
    """
    Class that samples and selects contact points for a planar object.
    Arguments: PlanarObject planar_object encoding surface and normal information
               list of GraspCriterium grasp_criteria
    """
    def __init__(self, planar_object, grasp_criteria=None):
        self.obj = planar_object
        self.criteria = grasp_criteria

        if self.criteria is None:
            self.criteria = [metrics.force_closure, metrics.rnorm, metrics.turtlebot_spacing]

    def plan_grasp(self, desired_twist, num_contacts, num_samples=1000):
        """
        Returns a (num_contacts, 2) contact set ndarray
        """
        # print('desired_twist: {0}'.format(desired_twist))
        candidate_grasps = self.sample_grasps(desired_twist, num_contacts, num_samples)
        if candidate_grasps is None:
            return None
        else:
            return candidate_grasps[self.rank_grasps(candidate_grasps, desired_twist)]

    def sample_grasps(self, desired_twist, num_contacts, num_samples):
        """
        Input: int number of contacts
               int number of samples
        Output: (num_samples, num_contacts, 2) ndarray of contact sets
        """
        # self._print("Sampling {0} candidate grasps...".format(num_samples))
        grasps = None
        for _ in range(num_samples):
            poses = self.obj.select_contacts(desired_twist, num_contacts)
            if poses is None:
                continue
            grasp = poses[:, :2].reshape((1, -1, 2))
            if grasps is None:
                grasps = grasp
            else:
                grasps = np.vstack((grasps, grasp))
        return grasps

    def rank_grasps(self, grasps, desired_twist):
        scores = self.evaluate_grasps(grasps, desired_twist)
        try:
            summed_scores = np.sum(scores, axis=1)
        except:
            print(scores)
            sys.exit(1)
        return np.argmax(summed_scores)

    def evaluate_grasps(self, candidate_grasps, desired_twist):
        """
        Input: (num_samples, num_contacts, 2) ndarray of contact sets
        Output: (num_samples, n) ndarray of criteria scores for contact sets
        """
        scores = None
        for criterium in self.criteria:
            criterium_scores = criterium.evaluate(candidate_grasps, desired_twist, self.obj)
            if scores is None:
                scores = criterium_scores
            else:
                scores = np.hstack((scores, criterium_scores))
        return scores

    def _print(self, msg):
        print("[GraspPlanner] {0}".format(msg))
