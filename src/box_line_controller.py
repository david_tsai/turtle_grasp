#!/usr/bin/env python

import numpy as np
import sys

import rospy
from geometry_msgs.msg import Twist, TwistStamped, Vector3Stamped, PointStamped
import tf

from box import Box
from controller import Controller
from grasp_planner import GraspPlanner
from pid_controller import PIDController


class BoxLineController(Controller):
    def __init__(self, lkp, lki, lkd, akp, aki, akd, num_contacts):
        super(BoxLineController, self).__init__('obj')
        rospy.init_node('BoxLineController', anonymous=False)
        print('BoxLineController with lKp {0}, lKi {1}, lKd {2}, aKp {3}, aKi {4}, aKd {5}, # contacts {6}'.format(lkp, lki, lkd, akp, aki, akd, num_contacts))

        self.linear_controller = PIDController(lkp, lki, lkd, 2)
        self.angular_controller = PIDController(akp, aki, akd, 2)
        self.num_contacts = num_contacts

        self.obj = Box(0.28, 0.225, 0.5)
        self.checkpoints = []
        self.checkpoints.append(np.array([2, -2]))  # needs to be described in world coordinates
        self.threshold = np.array([0.1, 0.1])
        self.ticks = 0
        self.setup()

    def setup_auxillary(self):
        self.bot_publishers = []
        for i in range(self.num_contacts):
            bot_name = 'contact{0}'.format(i + 1)
            bot_publisher = rospy.Publisher('obj2{0}'.format(bot_name), TwistStamped, queue_size=10)
            self.bot_publishers.append(bot_publisher)

        self.current_checkpoint = 0
        self.contact_poses = None
        self.grasp_planner = GraspPlanner(self.obj)
        self.finger_twists = None

    def calculate_latest_command(self, cmd):
        # state: x, y, theta, x_dot, y_dot, theta_dot
        if self.state is None:
            return

        goal = self.checkpoints[self.current_checkpoint]
        error = goal - self.state[:2]

        if np.all(np.abs(error) < self.threshold):
            print('goal reached, desired: {0}, {1}; reached: {2}, {3}'.format(goal[0], goal[1], self.state[0], self.state[1]))
            self.current_checkpoint += 1
            if self.current_checkpoint >= len(self.checkpoints):
                self.reached_goal = True
                return
            goal = self.checkpoints[self.current_checkpoint]
            error = goal - self.state[:2]

        try:
            world_error_vector = Vector3Stamped()
            world_error_vector.header.frame_id = 'map'
            world_error_vector.header.stamp = rospy.Time()
            world_error_vector.vector.x = error[0]
            world_error_vector.vector.y = error[1]

            object_error_vector = self.listener.transformVector3('obj', world_error_vector)
            object_error = np.array([object_error_vector.vector.x, object_error_vector.vector.y])
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return

        desired_twist = self.linear_controller.output(object_error)
        desired_twist = np.append(desired_twist, 0)

        if self.contact_poses is None:
        # if self.contact_poses is None or not self.obj.can_produce_velocity(desired_twist, self.contact_poses):
            proposed_poses = self.grasp_planner.plan_grasp(desired_twist, self.num_contacts)
            if proposed_poses is None:
                if self.contact_poses is None:
                    while proposed_poses is None:
                        proposed_poses = self.grasp_planner.plan_grasp(desired_twist, self.num_contacts)
                    self.contact_poses = proposed_poses
            else:
                self.contact_poses = proposed_poses

        cmd.linear.x = desired_twist[0]
        cmd.linear.y = desired_twist[1]
        cmd.angular.z = desired_twist[2]

    def closing_auxillary(self):
        bot_cmd = TwistStamped()
        bot_cmd.header.frame_id = ('1' if self.reached_goal else '0')
        for i, bot_publisher in enumerate(self.bot_publishers):
            bot_publisher.publish(bot_cmd)

    def run_auxillary(self, cmd):
        desired_twist = np.array([cmd.linear.x, cmd.linear.y, cmd.angular.z])
        if self.ticks % 100 == 0:
            print('BoxLineController: desired_twist = {0}'.format(desired_twist))
        self.ticks += 1
        proposed_finger_twists = self.obj.get_finger_twists(desired_twist, self.contact_poses)
        if proposed_finger_twists is not None:
            self.finger_twists = proposed_finger_twists
        bot_cmd = TwistStamped()
        bot_cmd.header.frame_id = ('1' if self.reached_goal else '0')
        for i, bot_publisher in enumerate(self.bot_publishers):
            finger_twist = self.finger_twists[i]
            object_finger_twist = self.obj.transform_finger_twist_to_object_frame(finger_twist, self.contact_poses[i])

            try:
                object_vector = Vector3Stamped()
                object_vector.header.frame_id = 'obj'
                object_vector.header.stamp = rospy.Time()
                object_vector.vector.x = object_finger_twist[0]
                object_vector.vector.y = object_finger_twist[1]
                #object_vector.vector.z = object_finger_twist[2]
                object_vector.vector.z = 0

                world_vector = self.listener.transformVector3('map', object_vector)
                world_finger_twist = np.array([world_vector.vector.x, world_vector.vector.y, world_vector.vector.z])
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue

            contact_gain = 100
            obj_contact_pose = PointStamped()
            obj_contact_pose.header.frame_id = "obj"
            obj_contact_pose.header.stamp = rospy.Time()
            obj_contact_pose.point.x = self.contact_poses[i][0]
            obj_contact_pose.point.y = self.contact_poses[i][1]
            robot_contact_pose = self.listener.transformPoint("contact{0}".format(i + 1), obj_contact_pose)

            bot_cmd.twist.linear.x = world_finger_twist[0]
            bot_cmd.twist.linear.y = world_finger_twist[1]
            # bot_cmd.twist.angular.z = world_finger_twist[2]
            bot_cmd.twist.angular.z = world_finger_twist[2] + (contact_gain * robot_contact_pose.point.y)
            bot_publisher.publish(bot_cmd)

if __name__ == '__main__':
    if len(sys.argv) < 8:
        print('Need 7 arguments (lKp, lKi, lKd, aKp, aKi, aKd, # contacts)')
        sys.exit(1)
    operator = BoxLineController(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), int(sys.argv[7]))
    operator.run()
