#!/usr/bin/env python

import numpy as np
import rospy

class PIDController(object):
    def __init__(self, kp, ki, kd, shape, last_time=None):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.cumsum_error = np.zeros(shape)
        self.prev_error = np.zeros(shape)
        self.last_time = rospy.Time.now().nsecs if last_time is None else last_time

    def output(self, error, time=None):
        if time is None:
            time = rospy.Time.now().nsecs
        dtime = time - self.last_time
        self.last_time = time

        derror = (error - self.prev_error) / dtime if dtime > 0.0 else 0.0
        self.prev_error = error
        self.cumsum_error += dtime * error

        return self.kp * error + self.ki * self.cumsum_error + self.kd * derror
