#!/usr/bin/env python

import numpy as np
import sys

from geometry_msgs.msg import Twist
import rospy
import tf


class Controller(object):
    def __init__(self, name):
        self.name = name

    def calculate_latest_command(self, cmd):
        raise NotImplemented

    def collect_goal(self, msg):
        raise NotImplemented

    def collect_state(self):
        try:
            now = rospy.Time(0)
            self.listener.waitForTransform('map', self.name, now, rospy.Duration(4))
            t = self.listener.getLatestCommonTime("map", self.name)
            (trans, rot) = self.listener.lookupTransform('map', self.name, t)
            # print("received pose")
            (lin, ang)  = self.listener.lookupTwist(self.name, "map", t, rospy.Duration(0.025))
            # print("received twist")

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            print('failed to receive transformation')
            print(e)
            return
        euler = tf.transformations.euler_from_quaternion(rot)

        self.state = np.array([trans[0], trans[1], euler[2], lin[0], lin[1], ang[2]])

    def setup_auxillary(self):
        raise NotImplemented

    def setup(self):
        rospy.loginfo('To stop {0} CTRL + C'.format(self.name))
        rospy.on_shutdown(self.shutdown)

        self.setup_auxillary()

        self.state = None
        self.reached_goal = False
        self.listener = tf.TransformListener()

    def closing_auxillary(self):
        raise NotImplemented

    def shutdown(self):
        rospy.loginfo('Stop {0}'.format(self.name))
        self.closing_auxillary()
        rospy.sleep(1)

    def run_auxillary(self, cmd):
        raise NotImplemented

    def run(self):
        r = rospy.Rate(10)  # 10 Hz
        cmd = Twist()
        while not rospy.is_shutdown():
            self.collect_state()
            self.calculate_latest_command(cmd)
            if self.reached_goal:
                rospy.loginfo("Arrived at destination, exiting...")
                self.closing_auxillary()
                rospy.sleep(1)
                sys.exit()
            else:
                self.run_auxillary(cmd)
            r.sleep()
