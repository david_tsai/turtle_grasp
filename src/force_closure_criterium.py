
import numpy as np
from scipy.spatial import Delaunay

from criterium import Criterium

class ForceClosureCriterium(Criterium):
    def __init__(self):
        super(ForceClosureCriterium, self).__init__('force_closure', self.is_force_closure)

    def is_force_closure(self, contact_poses, desired_twist, obj):
        """
        Input: (num_contacts, 2) ndarray of a contact set
        Output: criteria score for contact
        """

        # a single contact cannot be force-closure

        if len(contact_poses) < 2:
            return 0

        object_friction_cone = None
        for contact_pose in contact_poses:
            friction_cone = obj.get_object_friction_cone(contact_pose)
            if object_friction_cone is None:
                object_friction_cone = friction_cone
            else:
                object_friction_cone = np.vstack((object_friction_cone, friction_cone))

        try:
            hull = self.get_convex_hull(object_friction_cone)
            if self.is_origin_in_convex_hull(hull):
                return 1
        except:
            pass
        return 0

    def get_convex_hull(self, friction_cones):
        hull = Delaunay(friction_cones, qhull_options='QJ Pp')
        return hull

    def is_origin_in_convex_hull(self, hull):
        return hull.find_simplex(np.array([0, 0, 0])) >= 0
