#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

def main(filename):
    df = pd.read_csv(filename)

    num_contacts = ((len(df.columns) - 1) / 6) - 1

    agents = ["obj"] + ['contact{0}'.format(i + 1) for i in range(num_contacts)]

    plt.figure()
    for agent in agents:
        x = df['{0}_x'.format(agent)]
        y = df['{0}_y'.format(agent)]
        plt.plot(x, y, label=agent)
    plt.xlabel('x position (m)')
    plt.ylabel('y position (m)')
    plt.title('Agent Trajectories')
    plt.legend()
    plt.grid()
    plt.savefig('{0}.png'.format(filename))

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Need 1 argument (filename)')
        sys.exit(1)
    main(sys.argv[1])
