
import numpy as np

class PlanarObject(object):
    """
    TBA
    """
    def __init__(self, friction_coeff):
        pass

    def select_contacts(self, desired_twist, num_contacts):
        raise NotImplemented

    def get_grasp_map(self, contact_poses):
        raise NotImplemented
