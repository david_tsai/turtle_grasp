#!/usr/bin/env python

import numpy as np
import sys

import rospy
from geometry_msgs.msg import PointStamped, Twist
import tf

from box_circle_controller import BoxCircleController
from box_line_controller import BoxLineController


def main(operator):
    ticks = 0
    # dry run, produces a contact pose
    cmd = Twist()
    while operator.state is None:
        operator.collect_state()
        if operator.state is None:
            print('trying again in 3 seconds')
            rospy.sleep(3)
    operator.calculate_latest_command(cmd)

    threshold = np.array([0.1, 0.03])
    # threshold = np.array([1, 1])
    listener = tf.TransformListener()

    desired_twist = np.array([cmd.linear.x, cmd.linear.y, cmd.angular.z])
    print('desired_twist: {0}, {1}, {2}'.format(cmd.linear.x, cmd.linear.y, cmd.angular.z))
    (x, _) = operator.obj.get_nnls_finger_twists(desired_twist, operator.contact_poses)
    print(operator.contact_poses)
    print(x)

    # spins until each Turtlebot is "alligned" with the chosen contact point on the surface of the box
    for i in range(operator.num_contacts):
        error = np.ones(2)
        while not np.all(np.abs(error) <= threshold):
            contact_pose = operator.contact_poses[i]
            bot_pose = collect_pose_in_object_frame('contact{0}'.format(i + 1), listener)
            if bot_pose is None:
                continue
            error = contact_pose - bot_pose
            if ticks % 1000 == 0:
                print('contact_pose: {0}, {1}; bot_pose: {2}, {3}'.format(contact_pose[0], contact_pose[1], bot_pose[0], bot_pose[1]))
            ticks += 1
        print('contact {0} alligned'.format(i + 1))

    # assuming the Turtlebot operators on live, commence object tracking
    print('beginning run')
    rospy.sleep(3)
    operator.run()

def collect_pose_in_object_frame(toFrame, listener):
    try:
        now = rospy.Time(0)
        listener.waitForTransform('map', toFrame, now, rospy.Duration(4))
        (trans, _) = listener.lookupTransform('map', toFrame, now)
        world_point = PointStamped()
        world_point.header.frame_id = 'map'
        world_point.header.stamp = rospy.Time()
        world_point.point.x = trans[0]
        world_point.point.y = trans[1]

        object_point = listener.transformPoint('obj', world_point)
        bot_pose = np.array([object_point.point.x, object_point.point.y])
        return bot_pose
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
        return None

if __name__ == '__main__':
    if len(sys.argv) < 9:
        print('Need 8 arguments (type, lKp, lKi, lKd, aKp, aKi, aKd, # contacts)')
        sys.exit(1)
    if sys.argv[1] == 'line':
        operator = BoxLineController(float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), float(sys.argv[7]), int(sys.argv[8]))
    elif sys.argv[1] == 'circle':
        operator = BoxCircleController(float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), float(sys.argv[7]), int(sys.argv[8]))
    else:
        print('invalid controller type name, please choose \'line\' or \'circle\'')
        sys.exit(1)
    main(operator)
