
import numpy as np

from criterium import Criterium

class TurtlebotSpacingCriterium(Criterium):
    def __init__(self):
        super(TurtlebotSpacingCriterium, self).__init__('turtlebot_spacing', self.is_wide_enough)

    def is_wide_enough(self, contact_poses, desired_twist, obj):
        """
        Input: (num_contacts, 2) ndarray of a contact set
        Output: criteria score for contact
        """
        if len(contact_poses) < 2:
            return 0

        for i, contact_pose_1 in enumerate(contact_poses):
            center_1 = self.get_turtlebot_center(contact_pose_1, obj)
            for j in range(i + 1, len(contact_poses)):
                contact_pose_2 = contact_poses[j]
                center_2 = self.get_turtlebot_center(contact_pose_2, obj)
                # if np.linalg.norm(center_1 - center_2) < .354:
                if np.linalg.norm(center_1 - center_2) < .45:
                    return -10
        return 0

    def get_turtlebot_center(self, contact_pose, obj):
        surface_normal = obj.get_surface_normal(contact_pose)
        center = contact_pose + (surface_normal * -0.177)
        return center
