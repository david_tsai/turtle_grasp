#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Based on Chris Campbell's tutorial from iforce2d.net:
http://www.iforce2d.net/b2dtut/top-down-car
"""

from .framework import (Framework, Keys, main)
import math
from math import sqrt

from Box2D import *


class TDGroundArea(object):
    """
    An area on the ground that the car can run over
    """

    def __init__(self, friction_modifier):
        self.friction_modifier = friction_modifier


class TDTire(object):

    def __init__(self, car, max_speed=0.65, max_drive_force=3.25,
                 turn_torque=15, max_lateral_impulse=0.065,
                 dimensions=(0.015, 0.038), density=1.0,
                 position=(0, 0)):

        world = car.body.world

        self.current_traction = 1
        self.turn_torque = turn_torque
        self.max_speed = max_speed
        self.max_drive_force = max_drive_force
        self.max_lateral_impulse = max_lateral_impulse
        self.ground_areas = []

        self.body = world.CreateDynamicBody(position=position)
        self.body.CreatePolygonFixture(box=dimensions, density=density)
        self.body.userData = {'obj': self}

    @property
    def forward_velocity(self):
        body = self.body
        current_normal = body.GetWorldVector((0, 1))
        return current_normal.dot(body.linearVelocity) * current_normal

    @property
    def lateral_velocity(self):
        body = self.body

        right_normal = body.GetWorldVector((1, 0))
        return right_normal.dot(body.linearVelocity) * right_normal

    def update_friction(self):
        impulse = -self.lateral_velocity * self.body.mass
        # if impulse.length > self.max_lateral_impulse:
        #     impulse *= self.max_lateral_impulse / impulse.length

        self.body.ApplyLinearImpulse(self.current_traction * impulse, self.body.worldCenter, True)

        aimp = 0.1 * self.current_traction * self.body.inertia * -self.body.angularVelocity
        self.body.ApplyAngularImpulse(aimp, True)

        current_forward_normal = self.forward_velocity
        current_forward_speed = current_forward_normal.Normalize()

    def update_drive(self, keys, flag):
        if 'up' in keys:
            desired_speed = self.max_speed
        elif 'down' in keys:
            desired_speed = -self.max_speed

        if 'left' in keys:
            if 'up' in keys:
                if flag == 'left':
                    desired_speed = self.max_speed / 2
            elif 'down' in keys:
                if flag == 'left':
                    desired_speed = -self.max_speed / 2
            else:
                if flag == 'left':
                    desired_speed = -self.max_speed
                elif flag == 'right':
                    desired_speed = self.max_speed
        elif 'right' in keys:
            if 'up' in keys:
                if flag == 'right':
                    desired_speed = self.max_speed / 2
            elif 'down' in keys:
                if flag == 'right':
                    desired_speed = -self.max_speed / 2
            else:
                if flag == 'left':
                    desired_speed = self.max_speed
                elif flag == 'right':
                    desired_speed = -self.max_speed

        if not ('up' in keys or 'down' in keys or 'left' in keys or 'right' in keys):
            return

        # find the current speed in the forward direction
        current_forward_normal = self.body.GetWorldVector((0, 1))
        current_speed = self.forward_velocity.dot(current_forward_normal)

        # apply necessary force
        force = 0.0
        if desired_speed > current_speed:
            force = self.max_drive_force
        elif desired_speed < current_speed:
            force = -self.max_drive_force
        else:
            return

        self.body.ApplyForce(self.current_traction * force * current_forward_normal,
                             self.body.worldCenter, True)

    def update_turn(self, keys):
        if 'left' in keys:
            desired_torque = self.turn_torque
        elif 'right' in keys:
            desired_torque = -self.turn_torque
        else:
            return

        self.body.ApplyTorque(desired_torque, True)

    def add_ground_area(self, ud):
        if ud not in self.ground_areas:
            self.ground_areas.append(ud)
            self.update_traction()

    def remove_ground_area(self, ud):
        if ud in self.ground_areas:
            self.ground_areas.remove(ud)
            self.update_traction()

    def update_traction(self):
        if not self.ground_areas:
            self.current_traction = 1
        else:
            self.current_traction = 0
            mods = [ga.friction_modifier for ga in self.ground_areas]

            max_mod = max(mods)
            if max_mod > self.current_traction:
                self.current_traction = max_mod

class TDBox(object):

    def __init__(self, world, dimensions=(0.5, 0.5), density=1.0, friction=1.0, position=(0, 2.5)):

        world = world

        self.current_traction = 1
        self.ground_areas = []

        self.body = world.CreateDynamicBody(position=position)
        self.body.CreatePolygonFixture(box=dimensions, density=density, friction=friction)
        self.body.userData = {'obj': self}

    @property
    def forward_velocity(self):
        body = self.body
        current_normal = body.GetWorldVector((0, 1))
        return current_normal.dot(body.linearVelocity) * current_normal

    @property
    def lateral_velocity(self):
        body = self.body
        right_normal = body.GetWorldVector((1, 0))
        return right_normal.dot(body.linearVelocity) * right_normal

    def update_friction(self):
        print('linear velocity: {0:.2f}, {1:.2f}'.format(*self.body.linearVelocity))
        print('forward velocity: {0:.2f}, {1:.2f}'.format(*self.forward_velocity))
        print('#' * 10)

        current_forward_normal = self.forward_velocity
        print('current_forward_normal: {0:.2f}, {1:.2f}'.format(*current_forward_normal))
        print('current_forward_normal: {0}'.format(sqrt(current_forward_normal[0]**2 + current_forward_normal[1]**2)))
        current_forward_speed = current_forward_normal.Normalize()
        forward_drag_force_magnitude = -2 * current_forward_speed
        forward_friction = self.current_traction * forward_drag_force_magnitude * current_forward_normal
        print('current_forward_normal: {0:.2f}, {1:.2f}'.format(*current_forward_normal))
        print('current_forward_speed: {0:.2f}'.format(current_forward_speed))
        print('forward_drag_force_magnitude: {0:.2f}'.format(forward_drag_force_magnitude))
        print('forward_friction: {0:.2f}, {1:.2f}'.format(*forward_friction))
        print('#' * 20)

        # current_normal = self.body.linearVelocity
        # print('current_normal: {0:.2f}, {1:.2f}'.format(*current_normal))
        # print('current_normal: {0}'.format(sqrt(current_normal[0]**2 + current_normal[1]**2)))
        # current_speed = current_normal.Normalize()
        # drag_force_magnitude = -2 * current_speed
        # friction = self.current_traction * drag_force_magnitude * current_normal
        # print('current_normal: {0:.2f}, {1:.2f}'.format(*current_normal))
        # print('current_speed: {0:.2f}'.format(current_speed))
        # print('drag_force_magnitude: {0:.2f}'.format(drag_force_magnitude))
        # print('friction: {0:.2f}, {1:.2f}'.format(*friction))
        # print('#' * 20)

        # print('friction: {0:.2f}, {1:.2f}'.format(*friction))
        # print('forward friction: {0:.2f}, {1:.2f}'.format(*forward_friction))
        # print('#' * 20)
        self.body.ApplyForce(forward_friction, self.body.worldCenter, True)

        angular_drag_force_magnitude = -2 * self.body.angularVelocity
        angular_friction = self.current_traction * angular_drag_force_magnitude
        self.body.ApplyTorque(angular_friction, True)

    def add_ground_area(self, ud):
        if ud not in self.ground_areas:
            self.ground_areas.append(ud)
            self.update_traction()

    def remove_ground_area(self, ud):
        if ud in self.ground_areas:
            self.ground_areas.remove(ud)
            self.update_traction()

    def update_traction(self):
        if not self.ground_areas:
            self.current_traction = 1
        else:
            self.current_traction = 0
            mods = [ga.friction_modifier for ga in self.ground_areas]

            max_mod = max(mods)
            if max_mod > self.current_traction:
                self.current_traction = max_mod


class Turtlebot(object):
    tire_anchors = [(-0.1, 0), (0.1, 0)]

    def __init__(self, world, tire_anchors=None, density=153.659, position=(0, 0.5), **tire_kws):
        radius = 0.177
        self.body = world.CreateDynamicBody(position=position)
        self.body.CreateFixture(b2FixtureDef(shape=b2CircleShape(radius=radius), density=density))
        self.body.userData = {'obj': self}

        self.tires = [TDTire(self, **tire_kws) for i in range(2)]

        if tire_anchors is None:
            anchors = Turtlebot.tire_anchors

        joints = self.joints = []
        for tire, anchor in zip(self.tires, anchors):
            j = world.CreateRevoluteJoint(bodyA=self.body,
                                          bodyB=tire.body,
                                          localAnchorA=anchor,
                                          # center of tire
                                          localAnchorB=(0, 0),
                                          enableMotor=False,
                                          maxMotorTorque=1000,
                                          enableLimit=True,
                                          lowerAngle=0,
                                          upperAngle=0,
                                          )

            tire.body.position = self.body.worldCenter + anchor
            joints.append(j)

    def update(self, keys, hz):
        for tire in self.tires:
            tire.update_friction()

        flags = ['left', 'right']
        for i, tire in enumerate(self.tires):
            tire.update_drive(keys, flags[i])

        # control steering
        lock_angle = math.radians(40.)
        # from lock to lock in 0.5 sec
        turn_speed_per_sec = math.radians(160.)
        turn_per_timestep = turn_speed_per_sec / hz
        desired_angle = 0.0

        if 'left' in keys:
            desired_angle = lock_angle
        elif 'right' in keys:
            desired_angle = -lock_angle

        # front_left_joint, front_right_joint = self.joints[2:4]
        # angle_now = front_left_joint.angle
        # angle_to_turn = desired_angle - angle_now
        #
        # # TODO fix b2Clamp for non-b2Vec2 types
        # if angle_to_turn < -turn_per_timestep:
        #     angle_to_turn = -turn_per_timestep
        # elif angle_to_turn > turn_per_timestep:
        #     angle_to_turn = turn_per_timestep
        #
        # new_angle = angle_now + angle_to_turn
        # # Rotate the tires by locking the limits:
        # front_left_joint.SetLimits(new_angle, new_angle)
        # front_right_joint.SetLimits(new_angle, new_angle)


class OneTurtlebot(Framework):
    name = 'One Turtlebot'
    description = 'Keys: accel = w, reverse = s, left = a, right = d'

    def __init__(self):
        super(OneTurtlebot, self).__init__()
        # Top-down -- no gravity in the screen plane
        self.world.gravity = (0, 0)

        self.key_map = {Keys.K_w: 'up',
                        Keys.K_s: 'down',
                        Keys.K_a: 'left',
                        Keys.K_d: 'right',
                        }

        # Keep track of the pressed keys
        self.pressed_keys = set()

        # The walls
        spacing = 5
        boundary = self.world.CreateStaticBody(position=(0, spacing))
        boundary.CreateEdgeChain([(-spacing, -spacing),
                                  (-spacing, spacing),
                                  (spacing, spacing),
                                  (spacing, -spacing),
                                  (-spacing, -spacing)]
                                 )

        # A couple regions of differing traction
        self.car = Turtlebot(self.world)
        # self.car2 = Turtlebot(self.world, position=(0, 4.5))
        self.cardboard = TDBox(self.world)
        traction = 5
        gnd = self.world.CreateStaticBody(userData={'obj': TDGroundArea(traction)})
        fixture = gnd.CreatePolygonFixture(box=(spacing, spacing, (0, spacing), 0))
        # Set as sensors so that the car doesn't collide
        fixture.sensor = True

    def Keyboard(self, key):
        key_map = self.key_map
        if key in key_map:
            self.pressed_keys.add(key_map[key])
        else:
            super(OneTurtlebot, self).Keyboard(key)

    def KeyboardUp(self, key):
        key_map = self.key_map
        if key in key_map:
            self.pressed_keys.remove(key_map[key])
        else:
            super(OneTurtlebot, self).KeyboardUp(key)

    def handle_contact(self, contact, began):
        # A contact happened -- see if a wheel hit a
        # ground area
        fixture_a = contact.fixtureA
        fixture_b = contact.fixtureB

        body_a, body_b = fixture_a.body, fixture_b.body
        ud_a, ud_b = body_a.userData, body_b.userData
        if not ud_a or not ud_b:
            return

        tire = None
        ground_area = None
        for ud in (ud_a, ud_b):
            obj = ud['obj']
            if isinstance(obj, TDTire) or isinstance(obj, TDBox):
                tire = obj
            elif isinstance(obj, TDGroundArea):
                ground_area = obj

        if ground_area is not None and tire is not None:
            if began:
                tire.add_ground_area(ground_area)
            else:
                tire.remove_ground_area(ground_area)

    def BeginContact(self, contact):
        self.handle_contact(contact, True)

    def EndContact(self, contact):
        self.handle_contact(contact, False)

    def Step(self, settings):
        self.car.update(self.pressed_keys, settings.hz)
        # self.car2.update([], settings.hz)
        self.cardboard.update_friction()

        super(OneTurtlebot, self).Step(settings)

        tractions = [tire.current_traction for tire in self.car.tires]
        self.Print('Current tractions: %s' % tractions)

        position = self.car.body.worldCenter
        angle = self.car.body.angle
        self.Print('Current position: {0:0.3f} m, {1:0.3f} m, {2:0.3f} rad'.format(position[0], position[1], angle))

        linear_velocity = self.car.body.linearVelocity
        angular_velocity = self.car.body.angularVelocity
        self.Print('Current velocity: {0:0.3f} m/s, {1:0.3f} m/s, {2:0.3f} rad/s'.format(linear_velocity[0], linear_velocity[1], angular_velocity))

if __name__ == '__main__':
    main(OneTurtlebot)
