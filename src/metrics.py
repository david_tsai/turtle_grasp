
from force_closure_criterium import ForceClosureCriterium
from rnorm_criterium import RNormCriterium
from turtlebot_spacing_criterium import TurtlebotSpacingCriterium

force_closure = ForceClosureCriterium()
rnorm = RNormCriterium()
turtlebot_spacing = TurtlebotSpacingCriterium()
