#!/usr/bin/env python

import numpy as np
import sys

import rospy
from geometry_msgs.msg import Twist, TwistStamped, Vector3Stamped
import tf

from controller import Controller
from pid_controller import PIDController


class TurtlebotController(Controller):
    def __init__(self, name, lkp, lki, lkd, akp, aki, akd):
        super(TurtlebotController, self).__init__(name)
        rospy.init_node('TurtlebotController', anonymous=True)
        print('TurtlebotController {0} with lKp {1}, lKi {2}, lKd {3}, aKp {4}, aKi {5}, aKd {6}'.format(name, lkp, lki, lkd, akp, aki, akd))

        self.linear_controller = PIDController(lkp, lki, lkd, 3)
        self.angular_controller = PIDController(akp, aki, akd, 3)

        self.lin_cap = 0.1
        self.ang_cap = 0.6
        self.ticks = 0
        self.setup()

    def collect_goal(self, msg):
        self.desired_twist = np.array([msg.twist.linear.x, msg.twist.linear.y, msg.twist.angular.z])
        self.reached_goal = (msg.header.frame_id == '1')

    def setup_auxillary(self):
        self.cmd_vel = rospy.Publisher('/turtle_{0}/cmd_vel_mux/input/navi'.format(self.name[-1]), Twist, queue_size=10)
        rospy.Subscriber('obj2{0}'.format(self.name), TwistStamped, self.collect_goal)

        self.desired_twist = None

    def calculate_latest_command(self, cmd):
        # state: x, y, theta, x_dot, y_dot, theta_dot
        if self.state is None or self.desired_twist is None:
            return
        print(self.state)
        goal = self.desired_twist
        error = goal - self.state[3:]

        try:
            world_error_vector = Vector3Stamped()
            world_error_vector.header.frame_id = 'map'
            world_error_vector.header.stamp = rospy.Time()
            world_error_vector.vector.x = error[0]
            world_error_vector.vector.y = error[1]
            world_error_vector.vector.z = error[2]

            bot_error_vector = self.listener.transformVector3(self.name, world_error_vector)
            bot_error = np.array([bot_error_vector.vector.x, bot_error_vector.vector.y, bot_error_vector.vector.z])

            world_desried_vector = Vector3Stamped()
            world_desried_vector.header.frame_id = 'map'
            world_desried_vector.header.stamp = rospy.Time()
            world_desried_vector.vector.x = self.desired_twist[0]
            world_desried_vector.vector.y = self.desired_twist[1]
            bot_desired_vector = self.listener.transformVector3(self.name, world_desried_vector)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return

        angle_error = np.angle(bot_desired_vector.vector.x+bot_desired_vector.vector.y*1j)

        print('bot_error: {0}'.format(bot_error))
        linear_output = self.linear_controller.output(bot_error)
        bot_error[:2] = np.array([angle_error, 0])
        angular_output = self.angular_controller.output(bot_error)

        cmd.linear.x = np.linalg.norm(linear_output[:2])
        print('angular_output: {0}, {1}'.format(angular_output[0], angular_output[2]))
        # cmd.angular.z = angular_output[0] + angular_output[2]
        cmd.angular.z = angular_output[2]
        print('cmd: {0}, {1}'.format(cmd.linear.x, cmd.angular.z))

    def closing_auxillary(self):
        self.cmd_vel.publish(Twist())

    def run_auxillary(self, cmd):
        if cmd.linear.x > self.lin_cap:
            cmd.linear.x = self.lin_cap
        elif cmd.linear.x < -self.lin_cap:
            cmd.linear.x = -self.lin_cap
        if cmd.angular.z > self.ang_cap:
            cmd.angular.z = self.ang_cap
        elif cmd.angular.z < -self.ang_cap:
            cmd.angular.z = -self.ang_cap
        desired_cmd = np.array([cmd.linear.x, cmd.angular.z])
        if self.ticks % 100 == 0:
            print('TurtlebotController {0}: desired_cmd = {1}'.format(self.name, desired_cmd))
        self.ticks += 1
        self.cmd_vel.publish(cmd)

if __name__ == '__main__':
    if len(sys.argv) < 8:
        print('Need 7 arguments (name, lKp, lKi, lKd, aKp, aKi, aKd)')
        sys.exit(1)
    operator = TurtlebotController(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), float(sys.argv[7]))
    operator.run()
