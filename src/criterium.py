
import numpy as np

class Criterium(object):
    def __init__(self, name, scoring_fn):
        self.name = name
        self.scoring_fn = scoring_fn

    def evaluate(self, grasps, desired_twist, obj):
        """
        Input: (num_samples, num_contacts, 2) ndarray of contact sets
        Output: (num_samples, 1) ndarray of criteria scores for contact sets
        """
        scores = None
        for grasp in grasps:
            score = np.array([self.scoring_fn(grasp, desired_twist, obj)])
            if scores is None:
                scores = score
            else:
                scores = np.vstack((scores, score))
        return scores
