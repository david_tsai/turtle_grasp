#!/usr/bin/env python

import datetime
#import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import signal
import sys

import rospy
from geometry_msgs.msg import TwistStamped
import tf

from controller import Controller

class Plotter(Controller):
    def __init__(self, num_contacts):
        super(Plotter, self).__init__('plotter')
        rospy.init_node('Plotter', anonymous=False)

        self.num_contacts = num_contacts
        self.setup()

    def calculate_latest_command(self, cmd):
        if not self.begin_logging:
            return

        row = {}
        row['time'] = rospy.get_time()
        for i, agent in enumerate(self.agents):
            for j, column in enumerate(self.columns):
                if self.states[i] is None:
                    return
                row[agent + column] = self.states[i][j]
        self.row_list.append(row)

    def collect_goal(self, msg):
        self.begin_logging = True
        self.reached_goal = (msg.header.frame_id == '1')

    def collect_state(self):
        for i, agent in enumerate(self.agents):
            try:
                now = rospy.Time(0)
                self.listener.waitForTransform('map', agent, now, rospy.Duration(4))
                t = self.listener.getLatestCommonTime("map", agent)
                (trans, rot) = self.listener.lookupTransform('map', agent, t)
                (lin, ang)  = self.listener.lookupTwist(agent, "map", t, rospy.Duration(0.025))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                print('failed to receive transformation')
                print(e)
                continue
            euler = tf.transformations.euler_from_quaternion(rot)
            self.states[i] = np.array([trans[0], trans[1], euler[2], lin[0], lin[1], ang[2]])

    def setup_auxillary(self):
        rospy.Subscriber('obj2contact1', TwistStamped, self.collect_goal)

        self.begin_logging = False
        self.states = [None] * (self.num_contacts + 1)
        self.agents = ['obj'] + ['contact{0}'.format(i + 1) for i in range(self.num_contacts)]
        self.columns = ['_x', '_y', '_z', '_xd', '_yd', '_zd']
        self.row_list = []

    def closing_auxillary(self):
        time = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        self.log = pd.DataFrame(self.row_list)
#        plt.figure()
#        for agent in self.agents:
#            x, y = self.log[['{0}_x'.format(agent), '{0}_y'.format(agent)]]
#            plt.plot(x, y, label=agent)
#        plt.xlabel('x position (m)')
#        plt.ylabel('y position (m)')
#        plt.title('Agent Trajectories')
#        plt.legend()
#        plt.grid()
#        plt.savefig('{0}.png'.format(time))
        self.log.to_csv('{0}.csv'.format(time), index=False)
        print("Saved log to {0}.csv".format(time))

    def run_auxillary(self, cmd):
        pass

    def signal_handler(self, signal, frame):
        print('ending logging')
        self.closing_auxillary()
        sys.exit(0)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Need 1 argument (# contacts)')
        sys.exit(1)
    plotter = Plotter(int(sys.argv[1]))
    signal.signal(signal.SIGINT, plotter.signal_handler)
    plotter.run()
