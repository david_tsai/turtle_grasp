
import numpy as np

from criterium import Criterium

class RNormCriterium(Criterium):
    def __init__(self):
        super(RNormCriterium, self).__init__('rnorm', self.is_close_enough)

    def is_close_enough(self, contact_poses, desired_twist, obj):
        """
        Input: (num_contacts, 2) ndarray of a contact set
        Output: criteria score for contact
        """

        _, rnorm = obj.get_nnls_finger_twists(desired_twist, contact_poses)
        return -rnorm
