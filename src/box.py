
import numpy as np
import scipy.optimize as opt

from planar_object import PlanarObject


class Box(PlanarObject):
    """
    Container class containing all information needed to calculate the contact
    positions and respective wrenches required for pushing a box.
    All values are in box coordinates.
    """
    def __init__(self, x_length_m, y_length_m, friction_coeff):
        """
        Define a box in its own right-handed coordinate frame (x forward, y left)

                                        surfaces[1],
                    vertices[2]       normals[1]       vertices[1]
                                ----------------------
                                |          |           |
                                |          |           |
                                |          v           |
                                |                      |
        surfaces[2], normals[2] |--->              <---| surfaces[0], normals[0]
                                |                      |
                                |          ^           |
                                |          |           |
                                |          |           |
                                ----------------------
                    vertices[3]       surfaces[3],      vertices[0]
                                    normals[3]

        """
        super(Box, self).__init__(friction_coeff)
        self.vertices = np.array([[x_length_m/2,  -y_length_m/2],
                                  [x_length_m/2,   y_length_m/2],
                                  [-x_length_m/2,  y_length_m/2],
                                  [-x_length_m/2, -y_length_m/2]])
        # self.surfaces = np.array([self.vertices[1] - self.vertices[0],
        #                           self.vertices[2] - self.vertices[1],
        #                           self.vertices[3] - self.vertices[2],
        #                           self.vertices[0] - self.vertices[3], ])
        self.normals = np.array([[-1,  0],
                                 [0, -1],
                                 [1, 0],
                                 [0,  1]])
        self.mu = friction_coeff
        self.x_length_m = x_length_m
        self.y_length_m = y_length_m

        self.wrench_basis = np.array([[1, 0], [0, 1], [0, 0]])
        self.rotations = []
        self.rotations.append(np.array([[0, -1], [1, 0]]))
        self.rotations.append(np.array([[-1, 0], [0, -1]]))
        self.rotations.append(np.array([[0, 1], [-1, 0]]))
        self.rotations.append(np.array([[1, 0], [0, 1]]))
        self.rnorm_threshold = 3

    def is_acceptable_rnorm(self, rnorm):
        return rnorm <= self.rnorm_threshold

    def get_nnls_finger_twists(self, desired_twist, contact_poses):
        grasp_map = self.get_grasp_map(contact_poses)
        x, rnorm = opt.nnls(grasp_map, desired_twist)
        return (x, rnorm)

    def select_contacts(self, desired_twist, num_contacts):
        """
        All values are in box coordinates.
        Input: (3, ) ndarray of twist [lin_x, lin_y, ang]
               int number of contacts available
        Output: (num_contacts, 3) ndarray of contact poses, row [x, y, yaw]
        """
        contact_poses = self.get_random_contact_points(num_contacts)
        x, rnorm = self.get_nnls_finger_twists(desired_twist, contact_poses)

        if np.all(x == 0):
            return None

        for i in np.arange(0, num_contacts * 2, 2):
            if abs(x[i]) > (self.mu * x[i + 1]):
                return None

        # print('x: {0}, rnorm: {1}'.format(x, rnorm))
        if self.is_acceptable_rnorm(rnorm):
            return contact_poses
        else:
            return None

    def get_finger_twists(self, desired_twist, contact_poses):
        x, rnorm = self.get_nnls_finger_twists(desired_twist, contact_poses)
        if self.is_acceptable_rnorm(rnorm):
            finger_twists = None
            for i in range(len(contact_poses)):
                finger_twist = x[i*2:(i*2)+2]
                if finger_twists is None:
                    finger_twists = np.array([finger_twist])
                else:
                    finger_twists = np.vstack((finger_twists, finger_twist))
            return finger_twists
        else:
            return None

    def can_produce_velocity(self, desired_twist, contact_poses):
        x, rnorm = self.get_nnls_finger_twists(desired_twist, contact_poses)
        if self.is_acceptable_rnorm(rnorm):
            for i in np.arange(0, len(contact_poses) * 2, 2):
                if abs(x[i]) > (self.mu * x[i + 1]):
                    return False
            return True
        else:
            return False

    def get_random_contact_points(self, num_contacts):
        """
        All values are in box coordinates.
        Output: (num_contacts, 3) ndarray of a contact pose, row [x, y, yaw]
        """
        contact_poses = None
        perimeter_length_m = 2 * self.x_length_m + 2 * self.y_length_m
        perimeter_points = np.random.uniform(0, perimeter_length_m, num_contacts)
        for perimeter_point in perimeter_points:
            if perimeter_point <= self.y_length_m:
                contact_pose = np.array([self.x_length_m/2, perimeter_point - self.y_length_m/2, -np.pi/2])  # surface[0]
            elif perimeter_point > self.y_length_m and perimeter_point <= self.y_length_m + self.x_length_m:
                contact_pose = np.array([self.x_length_m/2 - (perimeter_point - self.y_length_m), self.y_length_m/2, np.pi])  # surface[1]
            elif perimeter_point > self.y_length_m + self.x_length_m and perimeter_point <= 2 * self.y_length_m + self.x_length_m:
                contact_pose = np.array([-self.x_length_m/2, self.y_length_m/2 - ((perimeter_point - (self.y_length_m + self.x_length_m))), np.pi/2])  # surface[2]
            elif perimeter_point > 2 * self.y_length_m + self.x_length_m and perimeter_point <= 2 * self.y_length_m + 2 * self.x_length_m:
                contact_pose = np.array([(perimeter_point - (2 * self.y_length_m + self.x_length_m)) - self.x_length_m/2, -self.y_length_m/2, 0])  # surface[3]

            if contact_poses is None:
                contact_poses = contact_pose.reshape((1, -1))
            else:
                contact_poses = np.vstack((contact_poses, contact_pose.reshape((1, -1))))
        return contact_poses

    def get_rotation_matrix(self, contact_pose):
        if contact_pose[0] == self.x_length_m/2:
            return self.rotations[0]
        elif contact_pose[1] == self.y_length_m/2:
            return self.rotations[1]
        elif contact_pose[0] == -self.x_length_m/2:
            return self.rotations[2]
        elif contact_pose[1] == -self.y_length_m/2:
            return self.rotations[3]

    def get_surface_normal(self, contact_pose):
        if contact_pose[0] == self.x_length_m/2:
            return self.normals[0]
        elif contact_pose[1] == self.y_length_m/2:
            return self.normals[1]
        elif contact_pose[0] == -self.x_length_m/2:
            return self.normals[2]
        elif contact_pose[1] == -self.y_length_m/2:
            return self.normals[3]

    def get_grasp_map(self, contact_poses):
        grasp_map = None
        for contact_pose in contact_poses:
            adjoint_transpose = self.get_adjoint_transpose(contact_pose)
            contact_map = np.dot(adjoint_transpose, self.wrench_basis)
            if grasp_map is None:
                grasp_map = contact_map
            else:
                grasp_map = np.hstack((grasp_map, contact_map))
        return grasp_map

    def get_adjoint_transpose(self, contact_pose):
        rot = self.get_rotation_matrix(contact_pose)
        trans = contact_pose[:2]
        adjoint_transpose = np.vstack((rot, np.dot(np.array([-trans[1], trans[0]]), rot)))
        adjoint_transpose = np.hstack((adjoint_transpose, np.array(np.array((0, 0, 1)).reshape((3, -1)))))
        return adjoint_transpose

    def get_object_friction_cone(self, contact_pose):
        corrected_adjoint_transpose = np.dot(self.get_adjoint_transpose(contact_pose), self.wrench_basis)
        contact_friction_cone =  np.array([[self.mu, 1], [-self.mu, 1]])
        object_friction_cone = np.dot(corrected_adjoint_transpose, contact_friction_cone.T).T
        return object_friction_cone

    def transform_finger_twist_to_object_frame(self, finger_twist, contact_pose):
        """
        All values are in box coordinates.
        Input: (3, ) ndarray of twist [lin_x, lin_y, ang] in contact frame
               (3, ) ndarray of a contact pose, row [x, y, yaw]
        Output: (3, ) ndarray of twist [lin_x, lin_y, ang] in object frame
        """
        rot = self.get_rotation_matrix(contact_pose)
        return np.dot(rot, finger_twist)
